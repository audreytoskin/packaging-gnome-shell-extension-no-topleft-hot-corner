# RPM packaging of the GNOME Shell extension No Topleft Hot Corner

This package spec is for No Topleft Hot Corner, which has its own GNOME Shell
[extension page](https://extensions.gnome.org/extension/118/no-topleft-hot-corner/)
and source code [repository](https://github.com/HROMANO/nohotcorner).

The top-left hot corner in GNOME Shell makes it easier to toggle the Activities
Overview --- perhaps a little too easy. No Topleft Hot Corner simply disables
the hot corner, so you don't have to worry about accidentally triggering
Activities anymore when moving the mouse near the top-left of the screen. But
you can still toggle the overview by clicking on the Activities button.



## Bug reports and feature requests

Report any issues with No Topleft Hot Corner itself on the project's
[GitHub](https://github.com/HROMANO/nohotcorner/issues).

Report issues specific to this package on the
[Red Hat Bugzilla](https://bugzilla.redhat.com/buglist.cgi?product=Fedora&component=gnome-shell-extension-no-topleft-hot-corner).



## License

Everything specific to this repository uses the MIT License.

No Topleft Hot Corner itself uses the GNU GPL version 2.
