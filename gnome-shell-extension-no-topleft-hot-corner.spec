Name:       gnome-shell-extension-no-topleft-hot-corner
Summary:    Disable the "hot corner" in the top-left of GNOME Shell
Version:    19.0
Release:    5%{?dist}
URL:        https://extensions.gnome.org/extension/118/no-topleft-hot-corner/
License:    GPLv2
BuildArch:  noarch

Source0: https://github.com/HROMANO/nohotcorner/archive/v%{version}/nohotcorner-%{version}.tar.gz

Requires: gnome-shell >= 3.8
Requires: gnome-shell-extension-common

# CentOS 7 build environment doesn't have new enough version of RPM to support Recommends tag.
%if 0%{?fedora}  ||  0%{?rhel} >= 8
Recommends: ( gnome-tweaks  or  gnome-tweak-tool )
Recommends: ( gnome-extensions-app  or  %{_bindir}/gnome-shell-extension-prefs )
%endif

# NOTE: Package is obsolete in GNOME 3.34+, where you can now disable the hot corners from GNOME Tweaks.
%if 0%{?fedora} >= 32  ||  0%{?rhel} >= 9
Provides: deprecated()
%endif


%description
The top-left hot corner in GNOME Shell makes it easier to toggle the Activities
Overview --- perhaps a little too easy. No Topleft Hot Corner simply disables
the hot corner, so you don't have to worry about accidentally triggering
Activities anymore when moving the mouse near the top-left of the screen. But
you can still toggle the overview by clicking on the Activities button.



# UUID is defined in extension's metadata.json and used as directory name.
%global  UUID                  nohotcorner@azuri.free.fr
%global  gnome_extensions_dir  %{_datadir}/gnome-shell/extensions/
%global  final_install_dir     %{buildroot}/%{gnome_extensions_dir}/%{UUID}

%prep
%autosetup -n nohotcorner-%{version}

cat > ./README-fedora.md << EOF
After installing, each user that wants it must still manually enable
No Topleft Hot Corner before it will take effect. You can do so a few
different ways.

First, restart GNOME Shell (Open the command dialog with Alt-F2, type
\`r\`, and hit enter), or log out and log back in. Then:

- If you've already set up the GNOME Shell web browser plugin, go to
  <https://extensions.gnome.org/local/>, find the extension, and click
  the switch to "ON."
- Open GNOME Tweaks, go to the Extensions tab, find the extension,
  and click the switch to "ON."
- Open a terminal or the desktop's command dialog, and (as your normal
  user account) run:
  \`gnome-extensions enable %{UUID}\`
EOF



%build
# No compiling necessary.



%install
mkdir -p %{final_install_dir}
cp --recursive --preserve=mode,timestamps  ./*  %{final_install_dir}
# LICENSE and README also get copied to standard documentation and license
# locations, so we don't need them here.
rm  %{final_install_dir}/LICENSE  %{final_install_dir}/README.md



%files
%doc README.md  README-fedora.md
%license LICENSE
%{gnome_extensions_dir}/%{UUID}/



%changelog
* Thu Apr 2 2020 Audrey Toskin <audrey@tosk.in> - 19.0-5
- Deprecate in Fedora 32+. Since GNOME 3.34, you can now disable the hot
  corner using GNOME Tweaks, so this Shell extension is now obsolete.

* Thu Apr 2 2020 Audrey Toskin <audrey@tosk.in> - 19.0-4
- Add explicit Recommend for gnome-shell-extension-prefs, and build for
  EPEL8.

* Tue Jan 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 19.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

* Thu Jul 25 2019 Fedora Release Engineering <releng@fedoraproject.org> - 19.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Tue Mar 26 2019 Audrey Toskin <audrey@tosk.in> - 19.0-1
- Bump to upstream version 19.0, which just assures compatibility with GNOME 3.32

* Thu Jan 31 2019 Fedora Release Engineering <releng@fedoraproject.org> - 17.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild
